/**
 * Author:  Alessandro Bianco
 * Website: http://alessandrobianco.eu
 * Twitter: @alebianco
 * Created: 26/07/12 8.32
 *
 * Copyright © 2011 - 2013 Alessandro Bianco
 */
package eu.alebianco.robotlegs.utils.impl {

import eu.alebianco.robotlegs.utils.api.IAsyncCommand;

import robotlegs.bender.framework.api.IContext;

public class AsyncCommand implements IAsyncCommand {
    protected var listeners:Array;
    private var _canceled:Boolean;

    [Inject]
    public var context:IContext;

    public function registerCompleteCallback(listener:Function):void {
        if (_canceled || listener == null) return;
        listeners ||= [];
        listeners.unshift(listener);
    }

    public function execute():void {
        if (_canceled) return;
        context.detain(this);
    }

    public function dispatchComplete(success:Boolean, data:Object = null):void {
        context.release(this);
        if (!_canceled)
        {
            for each (var listener:Function in listeners)
            {
                if (listener.length == 2)
                {
                    listener(success, data);
                } else if (listener.length == 1)
                {
                    listener(success);
                } else if (listener.length == 0)
                {
                    listener();
                } else
                {
                    throw new Error("Invalid number of arguments");
                }
            }
        }
        if (listeners) {
            listeners.length = 0;
            listeners = null;
        }
    }

    public function cancel():void {
        if (_canceled) {
            return; // avoid cancel again
        }

        _canceled = true;
        if (listeners) {
            listeners.length = 0;
            listeners = null;
        }
        context.release(this);
    }

    public function get canceled():Boolean
    {
        return _canceled;
    }
}
}
