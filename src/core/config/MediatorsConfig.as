package core.config
{

    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.starling.extensions.mediatorMap.api.IMediatorMap;

    import view.controls.ControlsView;
    import view.controls.ControlsViewMediator;
    import view.controls.buttons.ButtonsView;
    import view.controls.buttons.ButtonsViewMediator;

    import view.field.PlayFieldView;
    import view.field.PlayFieldViewMediator;
    import view.menu.StartMenuView;
    import view.menu.StartMenuViewMediator;
    import view.prompt.PromptView;
    import view.prompt.PromptViewMediator;

    import view.root.RootView;
    import view.root.RootViewMediator;

    public class MediatorsConfig implements IConfig
    {
        [Inject]
        public var mediatorMap:IMediatorMap;

        public function configure():void
        {
            trace("mediators config");
            mediatorMap.map(RootView).toMediator(RootViewMediator);
            mediatorMap.map(PlayFieldView).toMediator(PlayFieldViewMediator);
            mediatorMap.map(ControlsView).toMediator(ControlsViewMediator);
            mediatorMap.map(ButtonsView).toMediator(ButtonsViewMediator);
            mediatorMap.map(StartMenuView).toMediator(StartMenuViewMediator);
            mediatorMap.map(PromptView).toMediator(PromptViewMediator);
        }
    }
}
