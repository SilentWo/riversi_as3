package core.config
{
    import controller.commands.InitApplicationCommandSequence;
    import controller.commands.StartGameCommandSequence;
    import controller.commands.step.MakeStepCommandSequence;
    import controller.events.GameEvent;

    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.framework.api.IConfig;

    public class CommandsConfig implements IConfig
    {
        [Inject]
        public var commandMap:IEventCommandMap;

        public function configure():void
        {
            trace("commands config");
            commandMap.map(GameEvent.INIT_APPLICATION, GameEvent).toCommand(InitApplicationCommandSequence);
            commandMap.map(GameEvent.START_GAME, GameEvent).toCommand(StartGameCommandSequence);
            commandMap.map(GameEvent.MAKE_STEP, GameEvent).toCommand(MakeStepCommandSequence);
        }
    }
}
