package core.config
{
    import controller.playfield.IPlayFieldObjectManager;
    import controller.playfield.PlayFieldObjectsManager;

    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IInjector;

    import util.AssetUtil;
    import util.IAssetUtil;

    import view.field.cells.CellsFactory;

    import view.field.cells.ICellsFactory;

    public class ManagersConfig implements IConfig
    {
        [Inject]
        public var injector:IInjector;

        public function configure():void
        {
            trace("managers config");
            injector.map(IAssetUtil).toSingleton(AssetUtil, true);
            injector.map(ICellsFactory).toSingleton(CellsFactory, true);
            injector.map(IPlayFieldObjectManager).toSingleton(PlayFieldObjectsManager, true);
        }
    }
}
