package core.config
{
    import model.ai.AIModel;
    import model.ai.AIProcessor;
    import model.ai.IAIModel;
    import model.ai.IAIProcessor;
    import model.game.PlayFieldModel;

    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IInjector;

    public class ModelsConfig implements IConfig
    {
        [Inject]
        public var injector:IInjector;

        public function configure():void
        {
            trace("models config");
            injector.map(PlayFieldModel).asSingleton(true);
            injector.map(IAIModel).toSingleton(AIModel);
            injector.map(IAIProcessor).toSingleton(AIProcessor);
        }
    }
}
