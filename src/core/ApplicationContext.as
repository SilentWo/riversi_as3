package core
{
    import core.config.CommandsConfig;
    import core.config.ManagersConfig;
    import core.config.MediatorsConfig;
    import core.config.ModelsConfig;

    import flash.display.Stage;

    import robotlegs.bender.framework.impl.Context;
    import robotlegs.starling.extensions.contextView.ContextView;

    import starling.core.Starling;
    import starling.events.Event;

    import view.root.RootView;

    public class ApplicationContext
    {
        private var _starling:Starling;
        private var _context:Context;
        private var _starlingInitiated:Boolean = false;
        private var _contextInitiated:Boolean = false;

        public function ApplicationContext(stage:Stage)
        {
            RootView.stageWidth = stage.stageWidth;
            RootView.stageHeight = stage.stageHeight;

            _context = new Context();
            _context.install(ApplicationBundle);
            _context.afterInitializing(onContextCreatedHandler);
            _context.configure(
                    CommandsConfig,
                    ModelsConfig,
                    ManagersConfig,
                    MediatorsConfig
            );

            _starling = new Starling(RootView, stage);
            _starling.addEventListener(Event.ROOT_CREATED, onStarlingRootCreated);
            _starling.start();

            _context.configure(new ContextView(_starling));
        }

        private function onContextCreatedHandler():void
        {
            _contextInitiated = true;
            onStarlingAndContextCreated();
        }

        private function onStarlingRootCreated(event:Event):void
        {
            _starlingInitiated = true;
            onStarlingAndContextCreated();
        }

        private function onStarlingAndContextCreated():void
        {
            //todo something
        }
    }
}
