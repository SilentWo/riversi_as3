package util
{
    public class TextureName
    {
        public static const PLAYFIELD_FRAME:String = "field_frame";
        public static const BUTTON_RESTART:String = "button_restart";
        public static const BUTTON_CHOISE:String = "button_choise";
        public static const WINDOW_BG:String = "window_bg";
        public static const CHIP_BLACK:String = "chip_black";
        public static const CHIP_WHITE:String = "chip_white";
        public static const CHIP_ALLOW:String = "chip_allow";
        public static const GRAY_LINE:String = "gray_line";
    }
}
