package util
{
    import flash.display3D.Context3DTextureFormat;
    import flash.geom.Rectangle;

    import starling.display.Image;
    import starling.text.BitmapFont;
    import starling.textures.Texture;
    import starling.utils.AssetManager;

    public class AssetUtil extends AssetManager implements IAssetUtil
    {
        private var _greenScaleFont:BitmapFont;

        public function AssetUtil()
        {
            textureFormat = Context3DTextureFormat.BGRA;
            scaleFactor = 1;
        }

        public function getScale9Image(name:String, scaleRect:Rectangle = null):Image
        {
            try
            {
                var texture:Texture = getTexture(name);
                scaleRect ||= getScale9RectFromTexture(texture);
                var image:Image = new Image(texture);
                image.scale9Grid = scaleRect;
                return image;
            } catch (error:Error)
            {
                throw new Error(name + " -- " + error.toString());
            }
            return null;
        }

        private function getScale9RectFromTexture(texture:Texture):Rectangle
        {
            var horizontalPoint:Number = texture.width * 0.4;
            var verticalPoint:Number = texture.height * 0.4;
            return new Rectangle(horizontalPoint,
                    verticalPoint,
                    texture.width - horizontalPoint * 2,
                    texture.height - verticalPoint * 2);
        }
    }
}
