package util
{
    import flash.geom.Rectangle;

    import starling.display.Image;

    import starling.textures.Texture;

    public interface IAssetUtil
    {
        function getTexture(name:String):Texture;

        function getScale9Image(name:String, scaleRect:Rectangle = null):Image;
    }
}
