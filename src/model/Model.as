package model
{

    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;

    import robotlegs.bender.framework.api.IInjector;
    import robotlegs.bender.framework.api.ILogger;

    public class Model extends EventDispatcher
    {
        [Inject]
        public var injector:IInjector;
        [Inject]
        public var eventDispatcher:IEventDispatcher;

        public function Model()
        {

        }

        [PostConstruct]
        public function initialize():void
        {
            // override this method with [PostConstruct]
        }

        [PreDestroy]
        public function destroy():void
        {
            // override this method with [PreDestroy]
        }
    }
}
