package model.consts
{
    import model.game.cell.*;

    public class CellState
    {
        public static const WHITE:int = 1;
        public static const BLACK:int = 2;
        public static const EMPTY:int = 0;
        public static const ALLOW:int = 4; // allow step

        public static function validCell(cell:CellModel):Boolean
        {
            return validSide(cell.state);
        }

        public static function validSide(side:int):Boolean
        {
            return side == WHITE || side == BLACK;
        }

        public static function oppositeSide(side:int):int
        {
            if (!validSide(side)) throw new ArgumentError("invalid side value");

            return side == WHITE ? BLACK : WHITE;
        }
    }
}
