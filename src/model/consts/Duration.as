package model.consts
{
    public class Duration
    {
        public static const HIDE_LIGHTS:Number = 1;
        public static const SHOW_LIGHTS:Number = .5;
        public static const SPAWN_CHIP:Number = .5;
        public static const CHANGE_CHIP:Number = 1;
        public static const BETWEEN_CHANGE_CHIP:Number = .2;
    }
}
