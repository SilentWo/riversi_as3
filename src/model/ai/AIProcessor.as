package model.ai
{
    import model.game.PlayFieldModel;
    import model.game.cell.CellModel;

    import robotlegs.bender.framework.api.IInjector;

    public class AIProcessor implements IAIProcessor
    {
        [Inject]
        public var playFieldModel:PlayFieldModel;
        [Inject]
        public var injector:IInjector;

        private var _calculationPlayFieldModel:PlayFieldModel;

        public function getStepCell():CellModel
        {
            if (_calculationPlayFieldModel == null)
            {
                _calculationPlayFieldModel = new PlayFieldModel();
                _calculationPlayFieldModel.calcMode = true;
            }

            var cells:Vector.<Vector.<CellModel>> = playFieldModel.getCellsClone();
            _calculationPlayFieldModel.init(cells, playFieldModel.currentTurn);

            var stepCell:CellModel;
            var maxCharge:int = 0;

            for each (var cell:CellModel in _calculationPlayFieldModel.getAllowedStepCells())
            {
                var count:int = _calculationPlayFieldModel.getChangingCells(cell).length;

                if (count > maxCharge)
                {
                    maxCharge = count;
                    stepCell = cell;
                }
            }

            return stepCell != null ? playFieldModel.getCell(stepCell.x, stepCell.y) : null;
        }
    }
}

