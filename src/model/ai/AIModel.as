package model.ai
{
    public class AIModel implements IAIModel
    {
        private var _aiPlayers:Vector.<int> = new <int>[];

        public function addAIPlayer(side:int):void
        {
            _aiPlayers[_aiPlayers.length] = side;
        }

        public function isAIPlayer(side:int):Boolean
        {
            return _aiPlayers.indexOf(side) != -1;
        }

        public function clear():void
        {
            _aiPlayers.length = 0;
        }
    }
}
