package model.ai
{
    import model.game.cell.CellModel;

    public interface IAIProcessor
    {
        function getStepCell():CellModel;
    }
}
