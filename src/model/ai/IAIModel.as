package model.ai
{
    public interface IAIModel
    {
        function addAIPlayer(side:int):void;

        function isAIPlayer(side:int):Boolean;

        function clear():void;
    }
}
