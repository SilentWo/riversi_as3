package model.game.cell
{
    public class CellModel
    {
        private var _x:int;
        private var _y:int;
        private var _state:int;

        public function CellModel(x:int, y:int, state:int)
        {
            _x = x;
            _y = y;
            _state = state;
        }

        public function get x():int
        {
            return _x;
        }

        public function get y():int
        {
            return _y;
        }

        public function get state():int
        {
            return _state;
        }

        public function set state(value:int):void
        {
            _state = value;
        }

        public function toString():String
        {
            return "X: " + _x + " Y: " + _y + " St: " + _state;
        }
    }
}
