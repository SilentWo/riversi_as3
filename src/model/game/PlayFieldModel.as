package model.game
{
    import controller.events.GameEvent;

    import model.Model;
    import model.consts.CellState;
    import model.game.cell.CellModel;

    public class PlayFieldModel extends Model
    {
        public static const PLAYFIELD_SIZE:int = 8;

        private const WAY:Array = [
            /*   0° */   [1, 0],
            /*  45° */   [1, 1],
            /*  90° */   [0, 1],
            /* 135° */   [-1, 1],
            /* 180° */   [-1, 0],
            /* 225° */   [-1, -1],
            /* 270° */   [0, -1],
            /* 315° */   [1, -1]
        ];

        private var _cells:Vector.<Vector.<CellModel>> = new <Vector.<CellModel>>[];
        private var _currentTurn:int;
        private var _scoreWhite:int;
        private var _scoreBlack:int;
        private var _calcMode:Boolean;

        public function PlayFieldModel()
        {
            _calcMode = false;
        }

        public function init(cells:Vector.<Vector.<CellModel>> = null, currentTurn:int = CellState.BLACK):void
        {
            if (cells != null)
            {
                _cells = cells;
                _currentTurn = currentTurn;
            } else
            {
                for (var i:int = 0; i < PLAYFIELD_SIZE; i++)
                {
                    _cells[i] = new Vector.<CellModel>;
                    for (var j:int = 0; j < PLAYFIELD_SIZE; j++)
                    {
                        _cells[i][j] = new CellModel(i, j, CellState.EMPTY);
                    }
                }

                _currentTurn = CellState.BLACK;

                _cells[3][3].state = CellState.WHITE;
                _cells[3][4].state = CellState.BLACK;
                _cells[4][3].state = CellState.BLACK;
                _cells[4][4].state = CellState.WHITE;
            }

            analise();

            logging();
        }

        public function getChangingCells(cell:CellModel):Vector.<CellModel>
        {
            var result:Vector.<CellModel> = new <CellModel>[];

            for (var j:int = 0; j < WAY.length; j++)
            {
                var way:Array = WAY[j];
                var deltaX:int = way[0];
                var deltaY:int = way[1];
                var changingLine:Vector.<CellModel> = new <CellModel>[];
                var i:int = 0;

                while (true)
                {
                    i++;
                    var spotX:int = cell.x + deltaX * i;
                    var spotY:int = cell.y + deltaY * i;

                    if ((spotX >= PLAYFIELD_SIZE) || (spotY >= PLAYFIELD_SIZE) || (spotX < 0) || (spotY < 0))
                        break;    //out of field

                    var currentCell:CellModel = _cells[spotX][spotY];

                    if (currentCell.state == _currentTurn) // same cell reached
                    {
                        for each (var changingCell:CellModel in changingLine)
                        {
                            if (result.indexOf(changingCell) == -1)
                                result[result.length] = changingCell;
                        }
                        break;
                    }

                    if ((currentCell.state == CellState.EMPTY) || (currentCell.state == CellState.ALLOW))
                        break; // empty cell reached

                    if (currentCell.state == getOppositeTurn())
                        changingLine[changingLine.length] = currentCell;
                }
            }

            return result;
        }

        private function analise():void
        {
            for (var i:int = 0; i < _cells.length; i++)
            {
                for (var j:int = 0; j < _cells[i].length; j++)
                {
                    var cell:CellModel = _cells[i][j];
                    if (cell.state == CellState.ALLOW) cell.state = CellState.EMPTY;
                }
            }

            for (i = 0; i < _cells.length; i++)
            {
                for (j = 0; j < _cells[i].length; j++)
                {
                    cell = _cells[i][j];

                    var changingCells:Vector.<CellModel> = getChangingCells(cell);

                    if (cell.state == CellState.EMPTY && (changingCells.length > 0))
                    {
                        cell.state = CellState.ALLOW;
                    }
                }
            }

            _scoreWhite = 0;
            _scoreBlack = 0;

            for (var i:int = 0; i < _cells.length; i++)
            {
                for (var j:int = 0; j < _cells[i].length; j++)
                {
                    var cell:CellModel = _cells[i][j];

                    if (cell.state == CellState.BLACK)
                        _scoreBlack++;
                    if (cell.state == CellState.WHITE)
                        _scoreWhite++;
                }
            }

            if (!_calcMode && isDeadLock())
            {
                eventDispatcher.dispatchEvent(new GameEvent(GameEvent.DEADLOCK));
                switchStepOnDeadLock();
            }
        }

        public function switchStepOnDeadLock():void
        {
            _currentTurn = getOppositeTurn();
            analise();
        }

        public function makeStepAndGetChangingCells(stepCell:CellModel):Vector.<CellModel>
        {
            var changingCells:Vector.<CellModel> = getChangingCells(stepCell);
            for each (var cell:CellModel in changingCells)
            {
                if (cell.state == CellState.WHITE) cell.state = CellState.BLACK;
                else if (cell.state == CellState.BLACK) cell.state = CellState.WHITE;
            }

            stepCell.state = _currentTurn;

            _currentTurn = getOppositeTurn();

            analise();

            return changingCells;
        }

        public function getChipCells():Vector.<CellModel>
        {
            var result:Vector.<CellModel> = new <CellModel>[];
            for (var i:int = 0; i < _cells.length; i++)
            {
                for (var j:int = 0; j < _cells[i].length; j++)
                {
                    var cell:CellModel = _cells[i][j];
                    if (cell.state == CellState.BLACK || cell.state == CellState.WHITE)
                        result[result.length] = cell;
                }
            }

            return result;
        }

        public function getAllowedStepCells():Vector.<CellModel>
        {
            var result:Vector.<CellModel> = new <CellModel>[];
            for (var i:int = 0; i < _cells.length; i++)
            {
                for (var j:int = 0; j < _cells[i].length; j++)
                {
                    var cell:CellModel = _cells[i][j];
                    if (cell.state == CellState.ALLOW)
                        result[result.length] = cell;
                }
            }

            return result;
        }

        public function isFinishGame():Boolean
        {
            return (_scoreBlack + _scoreWhite) > PLAYFIELD_SIZE * PLAYFIELD_SIZE ||
                    _scoreBlack == 0 || _scoreWhite == 0;
        }

        public function getCellsClone():Vector.<Vector.<CellModel>>
        {
            return _cells.concat();
        }

        public function getCell(x:int, y:int):CellModel
        {
            return _cells[x][y];
        }

        private function isDeadLock():Boolean
        {
            var cells:Vector.<CellModel> = getAllowedStepCells();
            return !isFinishGame() && (cells == null || cells.length == 0);
        }

        private function getOppositeTurn():int
        {
            return _currentTurn == CellState.WHITE ? CellState.BLACK : CellState.WHITE;
        }

        private function logging():void
        {
            trace(">========CELLS=======>");
            for (var i:int = 0; i < PLAYFIELD_SIZE; i++)
            {
                var line:String = "";
                for (var j:int = 0; j < PLAYFIELD_SIZE; j++)
                {
                    line += _cells[i][j].state + "  ";
                }
                trace(line);
            }
            trace(">====================>");
        }

        public function get scoreBlack():int
        {
            return _scoreBlack;
        }

        public function get scoreWhite():int
        {
            return _scoreWhite;
        }

        public function get currentTurn():int
        {
            return _currentTurn;
        }

        public function set calcMode(value:Boolean):void
        {
            _calcMode = value;
        }
    }
}
