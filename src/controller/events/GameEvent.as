package controller.events
{
    import flash.events.Event;

    public class GameEvent extends DataEvent
    {
        public static const INIT_APPLICATION:String = "game_event_INIT_APPLICATION";
        public static const ON_INIT_APPLICATION_COMPLETE:String = "game_event_ON_INIT_APPLICATION_COMPLETE";
        public static const CREATE_GAME_VIEWS:String = "game_event_CREATE_GAME_VIEWS";
        public static const START_GAME:String = "game_event_START_GAME";
        public static const DEADLOCK:String = "game_event_DEADLOCK";
        public static const MAKE_STEP:String = "game_event_MAKE_STEP";
        public static const UPDATE_STATS:String = "game_event_UPDATE_SCORE";
        public static const SHOW_START_MENU:String = "game_event_SHOW_START_MENU";
        public static const HIDE_START_MENU:String = "game_event_HIDE_START_MENU";
        public static const SHOW_PROMPT:String = "game_event_HIDE_START_MENU";


        public function GameEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, data, bubbles, cancelable);
        }

        override public function clone():Event
        {
            return new GameEvent(type, data, bubbles, cancelable);
        }
    }
}
