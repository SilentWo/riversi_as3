package controller.events
{
    import flash.events.Event;

    public class PlayFieldEvent extends DataEvent
    {
        public static const SPAWN_CHIPS:String = "playField_event_SPAWN_CHIPS";
        public static const SPAWN_LIGHTS:String = "playField_event_SPAWN_LIGHTS";
        public static const CHANGE_CHIPS:String = "playField_event_CHANGE_CHIPS";
        public static const CLEAR_CHIPS:String = "playField_event_CLEAR_CHIPS";
        public static const CLEAR_LIGHTS:String = "playField_event_CLEAR_LIGHTS";

        public function PlayFieldEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, data, bubbles, cancelable);
        }

        override public function clone():Event
        {
            return new GameEvent(type, data, bubbles, cancelable);
        }
    }
}
