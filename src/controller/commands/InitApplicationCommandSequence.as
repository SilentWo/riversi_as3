package controller.commands
{
    import controller.events.GameEvent;

    import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

    import flash.events.IEventDispatcher;

    public class InitApplicationCommandSequence extends SequenceMacro
    {
        [Inject]
        public var eventDispatcher:IEventDispatcher;

        override public function prepare():void
        {
            add(LoadAssetsCommand);
            add(CreateGameViewsCommand);
            add(ShowMenuCommand);
            registerCompleteCallback(onComplete);
        }

        override public function execute():void
        {
            trace("[InitApplicationCommandSequence]: execute");
            super.execute();
        }

        private function onComplete(success:Boolean):void
        {
            if (success)
            {
                eventDispatcher.dispatchEvent(new GameEvent(GameEvent.ON_INIT_APPLICATION_COMPLETE));
                trace("[InitApplicationCommandSequence]: complete");
            } else
            {
                trace("[InitApplicationCommandSequence]: fault");
            }
        }
    }
}
