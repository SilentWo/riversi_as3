package controller.commands
{
    import controller.events.GameEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class CreateGameViewsCommand extends Command
    {
        [Inject]
        public var eventDispatcher:IEventDispatcher;

        override public function execute():void
        {
            trace("[CreateGameViewsCommand]: execute");
            super.execute();
            eventDispatcher.dispatchEvent(new GameEvent(GameEvent.CREATE_GAME_VIEWS));
        }
    }
}
