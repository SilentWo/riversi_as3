package controller.commands.step
{
    import controller.commands.step.ai.InterStepCommand;
    import controller.commands.step.result.IsFinishGameGuard;
    import controller.commands.step.result.ShowGameResultsCommand;
    import controller.events.GameEvent;
    import controller.events.PlayFieldEvent;

    import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

    import flash.events.IEventDispatcher;

    import model.game.PlayFieldModel;

    import model.game.cell.CellModel;

    import robotlegs.bender.extensions.directCommandMap.api.IDirectCommandMap;

    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

    public class MakeStepCommandSequence extends SequenceMacro
    {
        [Inject]
        public var event:GameEvent;
        [Inject]
        public var commandMap:IEventCommandMap;
        [Inject]
        public var directCommandMap:IDirectCommandMap;
        [Inject]
        public var playFieldModel:PlayFieldModel;

        private var _stepCell:CellModel;

        override public function prepare():void
        {
            _stepCell = CellModel(event.data);

            var changingCells:Vector.<CellModel> = playFieldModel.makeStepAndGetChangingCells(_stepCell);

            var allowedCells:Vector.<CellModel> = playFieldModel.getAllowedStepCells();

//            commandsMap.UnMap(GameEvent.MakeStep, typeof(MakeStepCommand));
//            commandsMap.UnMap(GameEvent.StartGame, typeof(StartGameCommandSequence));

            commandMap.unmap(GameEvent.MAKE_STEP, GameEvent).fromCommand(MakeStepCommandSequence);
            registerCompleteCallback(onCompleteHandler);

            add(HideLightsCommand);
            add(SpawnChipsCommand).withPayloads(new <CellModel>[_stepCell]);
            add(ChangeCellsCommand).withPayloads(changingCells);
            add(ShowLightsCommand).withPayloads(allowedCells);
            add(ShowScoreCommand);
            add(ShowGameResultsCommand).withGuards(IsFinishGameGuard);
        }

        override public function execute():void
        {
            trace("[MakeStepCommandSequence]: execute");
            super.execute();
        }

        private function onCompleteHandler(success:Boolean):void
        {
            trace("[MakeStepCommandSequence]: complete");
            commandMap.map(GameEvent.MAKE_STEP, GameEvent).toCommand(MakeStepCommandSequence);

            directCommandMap.map(InterStepCommand).execute();
        }
    }
}
