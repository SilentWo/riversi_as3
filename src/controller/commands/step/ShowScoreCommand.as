package controller.commands.step
{
    import controller.events.GameEvent;

    import flash.events.IEventDispatcher;

    import model.game.PlayFieldModel;

    import robotlegs.bender.bundles.mvcs.Command;

    public class ShowScoreCommand extends Command
    {
        [Inject]
        public var eventDispatcher:IEventDispatcher;
        [Inject]
        public var playFieldModel:PlayFieldModel;

        override public function execute():void
        {
            trace("[ShowScoreCommand]: execute");
            super.execute();

            eventDispatcher.dispatchEvent(new GameEvent(GameEvent.UPDATE_STATS, {
                blackScore: playFieldModel.scoreBlack,
                whiteScore: playFieldModel.scoreWhite,
                currentTurn: playFieldModel.currentTurn
            }));
        }
    }
}
