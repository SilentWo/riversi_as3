package controller.commands.step.result
{
    import model.game.PlayFieldModel;

    import robotlegs.bender.framework.api.IGuard;

    public class IsFinishGameGuard implements IGuard
    {
        [Inject]
        public var playFieldModel:PlayFieldModel;

        public function approve():Boolean
        {
            return playFieldModel.isFinishGame();
        }
    }
}
