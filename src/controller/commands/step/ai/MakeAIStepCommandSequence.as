package controller.commands.step.ai
{
    import controller.commands.step.MakeStepCommandSequence;
    import controller.events.GameEvent;

    import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

    import flash.events.IEventDispatcher;

    import model.ai.IAIProcessor;
    import model.game.cell.CellModel;

    public class MakeAIStepCommandSequence extends SequenceMacro
    {
        [Inject]
        public var aiProcessor:IAIProcessor;
        [Inject]
        public var eventDispatcher:IEventDispatcher;

        override public function prepare():void
        {
            var stepCell:CellModel = aiProcessor.getStepCell();

            eventDispatcher.dispatchEvent(new GameEvent(GameEvent.SHOW_PROMPT, {text: "ХОД ИИ"}));

            add(MakeStepCommandSequence).withPayloads(new GameEvent(GameEvent.MAKE_STEP, stepCell));
        }

        override public function execute():void
        {
            trace("[MakeStepCommandSequence]: execute");
            super.execute();
        }
    }
}
