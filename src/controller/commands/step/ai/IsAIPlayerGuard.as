package controller.commands.step.ai
{
    import model.ai.IAIModel;
    import model.consts.CellState;
    import model.game.PlayFieldModel;

    import robotlegs.bender.framework.api.IGuard;

    public class IsAIPlayerGuard implements IGuard
    {
        [Inject]
        public var playFieldModel:PlayFieldModel;
        [Inject]
        public var aiModel:IAIModel;

        public function approve():Boolean
        {
            return aiModel.isAIPlayer(CellState.oppositeSide(playFieldModel.currentTurn));
        }
    }
}
