package controller.commands.step.ai
{
    import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

    public class InterStepCommand extends SequenceMacro
    {
        override public function prepare():void
        {
            add(MakeAIStepCommandSequence).withGuards(IsAIPlayerGuard);
        }

        override public function execute():void
        {
            trace("[InterStepCommand]: execute");
            super.execute();
        }
    }
}
