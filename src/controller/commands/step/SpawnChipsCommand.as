package controller.commands.step
{
    import com.greensock.TweenLite;

    import controller.playfield.IPlayFieldObjectManager;

    import eu.alebianco.robotlegs.utils.impl.AsyncCommand;

    import model.consts.Duration;

    import model.game.cell.CellModel;

    public class SpawnChipsCommand extends AsyncCommand
    {
        [Inject]
        public var spawningCells:Vector.<CellModel>;
        [Inject]
        public var playFieldObjectsManager:IPlayFieldObjectManager;

        override public function execute():void
        {
            trace("[SpawnChipsCommand]: execute");
            super.execute();

            for each (var cell:CellModel in spawningCells)
            {
                playFieldObjectsManager.spawnChip(cell, Duration.SPAWN_CHIP);
            }

            TweenLite.delayedCall(Duration.HIDE_LIGHTS, complete);
        }

        private function complete():void
        {
            trace("[SpawnChipsCommand]: complete");
            dispatchComplete(true);
        }
    }
}
