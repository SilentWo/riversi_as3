package controller.commands.step
{
    import com.greensock.TweenLite;

    import controller.playfield.IPlayFieldObjectManager;

    import eu.alebianco.robotlegs.utils.impl.AsyncCommand;

    import model.consts.Duration;

    import model.game.cell.CellModel;

    public class ChangeCellsCommand extends AsyncCommand
    {
        [Inject]
        public var changingCells:Vector.<CellModel>;
        [Inject]
        public var playFieldObjectsManager:IPlayFieldObjectManager;

        override public function execute():void
        {
            trace("[ChangeCellsCommand]: execute");
            super.execute();

            var completeDuration:Number = (Duration.BETWEEN_CHANGE_CHIP + 1) * changingCells.length;
            TweenLite.delayedCall(completeDuration, complete);

            for (var i:int = 0; i < changingCells.length; i++)
            {
                TweenLite.delayedCall(Duration.BETWEEN_CHANGE_CHIP * i, delayedChange, [changingCells[i]]);
            }
        }

        private function delayedChange(cell:CellModel):void
        {
            playFieldObjectsManager.changeChip(cell, Duration.CHANGE_CHIP);
        }

        private function complete():void
        {
            trace("[ChangeCellsCommand]: complete");
            dispatchComplete(true);
        }
    }
}
