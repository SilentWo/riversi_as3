package controller.commands.step
{
    import com.greensock.TweenLite;

    import controller.playfield.IPlayFieldObjectManager;

    import eu.alebianco.robotlegs.utils.impl.AsyncCommand;

    import model.consts.Duration;

    import robotlegs.bender.bundles.mvcs.Command;

    public class HideLightsCommand extends Command

    {
        [Inject]
        public var playFieldObjectsManager:IPlayFieldObjectManager;

        override public function execute():void
        {
            trace("[HideLightsCommand]: execute");
            super.execute();

            playFieldObjectsManager.hideLights(Duration.HIDE_LIGHTS);
        }
    }
}
