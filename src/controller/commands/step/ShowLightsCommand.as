package controller.commands.step
{
    import com.greensock.TweenLite;

    import controller.playfield.IPlayFieldObjectManager;

    import eu.alebianco.robotlegs.utils.impl.AsyncCommand;

    import model.consts.Duration;

    import model.game.cell.CellModel;

    public class ShowLightsCommand extends AsyncCommand
    {
        [Inject]
        public var playFieldObjectsManager:IPlayFieldObjectManager;
        [Inject]
        public var allowedCells:Vector.<CellModel>;

        override public function execute():void
        {
            trace("[ShowLightsCommand]: execute");
            super.execute();

            for each (var cell:CellModel in allowedCells)
            {
                playFieldObjectsManager.spawnLight(cell, Duration.SHOW_LIGHTS);
            }

            TweenLite.delayedCall(Duration.HIDE_LIGHTS, complete);
        }

        private function complete():void
        {
            trace("[ShowLightsCommand]: complete");
            dispatchComplete(true);
        }
    }
}
