package controller.commands
{
    import eu.alebianco.robotlegs.utils.impl.AsyncCommand;

    import flash.events.IEventDispatcher;
    import flash.filesystem.File;

    import starling.utils.AssetManager;

    import util.IAssetUtil;

    public class LoadAssetsCommand extends AsyncCommand
    {
        [Inject]
        public var assetsUtil:IAssetUtil;
        [Inject]
        public var eventDispatcher:IEventDispatcher;

        override public function execute():void
        {
            trace("[LoadAssetsCommand]: execute");
            super.execute();

            var assetManager:AssetManager = assetsUtil as AssetManager;
            assetManager.verbose = true;

            var appDir:File = new File(File.applicationDirectory.nativePath).resolvePath("./../../../assets/");
            assetManager.enqueue(appDir.resolvePath("atlas.png"));
            assetManager.enqueue(appDir.resolvePath("atlas.xml"));
            assetManager.enqueue(appDir.resolvePath("greenScale.fnt"));
            assetManager.enqueue(appDir.resolvePath("mainFont.fnt"));

            assetManager.loadQueue(onLoadAssetProgress);
        }

        private function onLoadAssetProgress(ratio:Number):void
        {
            trace("Loading assets, progress:", ratio);

            // when the ratio equals '1', we are finished.
            if (ratio == 1.0)
            {
                trace("[LoadAssetsCommand]: complete");
                dispatchComplete(true)
            }
        }
    }
}
