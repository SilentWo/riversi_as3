package controller.commands
{
    import controller.events.GameEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class ShowMenuCommand extends Command
    {
        [Inject]
        public var eventDispatcher:IEventDispatcher;

        override public function execute():void
        {
            trace("[ShowMenuCommand]: execute");
            super.execute();
            
            eventDispatcher.dispatchEvent(new GameEvent(GameEvent.SHOW_START_MENU));
        }
    }
}
