package controller.commands
{
    import controller.commands.step.ShowLightsCommand;
    import controller.commands.step.SpawnChipsCommand;

    import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

    import model.game.PlayFieldModel;

    public class InitPlayFieldModelCommand extends SequenceMacro
    {
        [Inject]
        public var playFieldModel:PlayFieldModel;

        override public function prepare():void
        {
            playFieldModel.init();
            add(SpawnChipsCommand).withPayloads(playFieldModel.getChipCells());
            add(ShowLightsCommand).withPayloads(playFieldModel.getAllowedStepCells());
            registerCompleteCallback(onCompleteHandler);
        }

        override public function execute():void
        {
            trace("[CreateGameViewsCommand]: execute");
            super.execute();
        }

        private function onCompleteHandler(success:Boolean):void
        {
            trace("[CreateGameViewsCommand]: complete");
        }
    }
}
