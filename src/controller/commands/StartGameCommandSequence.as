package controller.commands
{
    import controller.commands.step.ShowScoreCommand;
    import controller.commands.step.ai.InterStepCommand;
    import controller.events.GameEvent;
    import controller.playfield.IPlayFieldObjectManager;
    import controller.playfield.PlayFieldObjectsManager;

    import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

    import model.ai.IAIModel;
    import model.consts.CellState;

    public class StartGameCommandSequence extends SequenceMacro
    {
        [Inject]
        public var event:GameEvent;
        [Inject]
        public var aiModel:IAIModel;
        [Inject]
        public var playFieldObjectsManager:IPlayFieldObjectManager;

        override public function prepare():void
        {
            playFieldObjectsManager.clearField();
            aiModel.clear();

            var players:int = event.data.players;
            var side:int = event.data.players;

            if (players == 1 && CellState.validSide(side))
                aiModel.addAIPlayer(CellState.oppositeSide(side));

            add(InitPlayFieldModelCommand);
            add(ShowScoreCommand);
            add(InterStepCommand);
        }

        override public function execute():void
        {
            trace("[StartGameCommandSequence]: execute");
            super.execute();
        }
    }
}
