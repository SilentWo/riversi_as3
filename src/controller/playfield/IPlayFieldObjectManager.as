package controller.playfield
{
    import model.game.cell.CellModel;

    import starling.display.DisplayObjectContainer;

    public interface IPlayFieldObjectManager
    {
        function init(container:DisplayObjectContainer, cellSize:Number):void

        function spawnChip(cell:CellModel, animationDuration:Number):void;

        function changeChip(cell:CellModel, animationDuration:Number):void;

        function spawnLight(cell:CellModel, animationDuration:Number):void

        function hideLights(animationDuration:Number):void;

        function clearField():void;
    }
}
