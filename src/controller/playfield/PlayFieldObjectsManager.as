package controller.playfield
{
    import com.greensock.TweenLite;

    import flash.utils.Dictionary;

    import model.consts.CellState;
    import model.game.cell.CellModel;

    import starling.display.DisplayObjectContainer;
    import starling.events.Event;

    import view.field.cells.CellView;
    import view.field.cells.ICellsFactory;

    public class PlayFieldObjectsManager implements IPlayFieldObjectManager
    {
        [Inject]
        public var cellFactory:ICellsFactory;

        private var _container:DisplayObjectContainer;
        private var _cellSize:Number;
        private var _halfCellSize:Number;
        private var _cellViews:Dictionary = new Dictionary();

        public function init(container:DisplayObjectContainer, cellSize:Number):void
        {
            cellFactory.setCellSize(_cellSize);
            _container = container;
            _cellSize = cellSize;
            _halfCellSize = cellSize * .5;
        }

        public function spawnChip(cell:CellModel, animationDuration:Number):void
        {
            var cellView:CellView = getCellView(cell);
            cellView.width = cellView.height = 1;
            TweenLite.to(cellView, animationDuration, {width: _cellSize, height: _cellSize});
        }

        public function changeChip(cell:CellModel, animationDuration:Number):void
        {
            var changingCellView:CellView;
            for each (var cellView:CellView in _cellViews)
            {
                if (cellView.model == cell)
                    changingCellView = cellView;
            }

            if (changingCellView == null) return;

            var halfDuration:Number = animationDuration * .5;

            TweenLite.to(changingCellView, halfDuration, {
                width: _cellSize * 2,
                height: _cellSize * 2,
                alpha: 0,
                onComplete: changeFadeIn,
                onCompleteParams: [changingCellView, halfDuration, cell]
            });
        }

        private function changeFadeIn(changingCellView:CellView, halfDuration:Number, cell:CellModel):void
        {
            changingCellView.setModel(cell);
            changingCellView.width = changingCellView.height = 1;
            changingCellView.alpha = 1;

            TweenLite.to(changingCellView, halfDuration, {
                width: _cellSize,
                height: _cellSize
            });
        }

        public function spawnLight(cell:CellModel, animationDuration:Number):void
        {
            var cellView:CellView = getCellView(cell);
            cellView.alpha = 0;
            TweenLite.to(cellView, animationDuration, {alpha: 1});
        }

        public function hideLights(animationDuration:Number):void
        {
            for each (var cellView:CellView in _cellViews)
            {
                if (cellView.model.state == CellState.ALLOW || cellView.model.state == CellState.EMPTY)
                    TweenLite.to(cellView, animationDuration, {
                        alpha: 0,
                        onComplete: onHideLightsComplete,
                        onCompleteParams: [cellView]
                    });
            }
        }

        private function onHideLightsComplete(cellView:CellView):void
        {
            delete _cellViews[cellView.model];
            cellFactory.releaseCellView(cellView);
        }

        public function clearField():void
        {
            for each (var cellView:CellView in _cellViews)
            {
                cellFactory.releaseCellView(cellView);
            }
        }

        private function getCellView(cell:CellModel):CellView
        {
            var previousCell:CellView = _cellViews[cell];
            if (previousCell != null)
            {
                cellFactory.releaseCellView(previousCell);
                delete _cellViews[previousCell];
            }

            var cellView:CellView = cellFactory.getCellView();
            cellView.setModel(cell);
            cellView.x = _halfCellSize + cell.x * _cellSize;
            cellView.y = _halfCellSize + cell.y * _cellSize;
            cellView.addEventListener(CellView.LIGHT_TOUCHED, onLightTouchedHandler);

            _cellViews[cell] = cellView;
            _container.addChild(cellView);

            return cellView;
        }

        private function onLightTouchedHandler(e:Event):void
        {
            var cellView:CellView = e.target as CellView;
            delete _cellViews[cellView.model];
            cellFactory.releaseCellView(cellView);
        }
    }
}
