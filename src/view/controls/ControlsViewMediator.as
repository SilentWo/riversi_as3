package view.controls
{
    import controller.events.GameEvent;

    import model.consts.CellState;

    import robotlegs.starling.bundles.mvcs.Mediator;
    import robotlegs.starling.extensions.mediatorMap.api.IMediatorMap;

    public class ControlsViewMediator extends Mediator
    {
        [Inject]
        public var view:ControlsView;
        [Inject]
        public var mediatorMap:IMediatorMap;

        override public function initialize():void
        {
            view.setTurnBlack();
            view.setScore(0, 0);

            mediatorMap.mediate(view.buttonsView);

            addContextListener(GameEvent.UPDATE_STATS, onUpdateScore);
        }

        private function onUpdateScore(e:GameEvent):void
        {
            view.setScore(e.data.blackScore, e.data.whiteScore);

            if (e.data.currentTurn == CellState.BLACK)
                view.setTurnBlack();
            else
                view.setTurnWhite();
        }
    }
}
