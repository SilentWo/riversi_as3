package view.controls.score
{
    import starling.display.Sprite;
    import starling.text.TextField;
    import starling.text.TextFieldAutoSize;
    import starling.text.TextFormat;
    import starling.utils.Color;

    import util.FontName;

    import view.root.RootView;

    public class ScoreView extends Sprite
    {
        private var _blackScore:TextField;
        private var _whiteScore:TextField;

        public function ScoreView()
        {
            var labelHeight:Number = 40;
            var textFormat:TextFormat = new TextFormat(FontName.MAIN_FONT, labelHeight, Color.WHITE);
            var caption:TextField = new TextField(RootView.stageWidth / 3, labelHeight, "СЧЕТ", textFormat);
            caption.alignPivot();
            caption.format.size = 25;
            caption.x = 95;
            caption.y = 30;
            addChild(caption);

            _blackScore = new TextField(RootView.stageWidth / 6, labelHeight, "00", textFormat);
            _blackScore.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
            _blackScore.format.color = Color.BLACK;
            addChild(_blackScore);

            _whiteScore = new TextField(RootView.stageWidth / 6, labelHeight, "00", textFormat);
            _whiteScore.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
            addChild(_whiteScore);
        }

        public function setScore(blackScore:int, whiteScore:int):void
        {
            _blackScore.text = blackScore.toString();
            _blackScore.alignPivot();
            _blackScore.x = 65;
            _blackScore.y = 75;

            _whiteScore.text = whiteScore.toString();
            _whiteScore.alignPivot();
            _whiteScore.x = 125;
            _whiteScore.y = _blackScore.y;
        }
    }
}
