package view.controls.turn
{
    import starling.text.TextField;
    import starling.text.TextFieldAutoSize;
    import starling.text.TextFormat;
    import starling.utils.Color;

    import util.FontName;

    import view.View;
    import view.root.RootView;

    public class TurnView extends View
    {
        private var _textField:TextField;

        [PostConstruct]
        override public function initialize():void
        {
            var labelHeight:Number = 23;
            var textFormat:TextFormat = new TextFormat(FontName.MAIN_FONT, labelHeight, Color.WHITE);

            _textField = new TextField(RootView.stageWidth / 3, labelHeight, "ХОД ЧЕРНЫХ", textFormat);
            _textField.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
            addChild(_textField);
        }

        public function setText(text:String):void
        {
            _textField.text = text;
            _textField.alignPivot();
            _textField.x = 95;
            _textField.y = 60;
        }

        public function setColor(color:uint):void
        {
            _textField.format.color = color;
        }
    }
}
