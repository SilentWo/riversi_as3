package view.controls.buttons
{
    import controller.events.GameEvent;

    import robotlegs.starling.bundles.mvcs.Mediator;

    import starling.events.Event;

    public class ButtonsViewMediator extends Mediator
    {
        [Inject]
        public var view:ButtonsView;

        override public function initialize():void
        {
            addViewListener(ButtonsView.ON_BUTTON_RESTART_TRIGGERED, onRestartTriggered);
        }

        private function onRestartTriggered(e:Event):void
        {
            dispatch(new GameEvent(GameEvent.SHOW_START_MENU));
        }
    }
}
