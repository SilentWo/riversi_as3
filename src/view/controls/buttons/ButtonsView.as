package view.controls.buttons
{
    import starling.display.Button;
    import starling.events.Event;
    import starling.text.TextFormat;
    import starling.utils.Color;

    import util.FontName;
    import util.IAssetUtil;
    import util.TextureName;

    import view.View;

    public class ButtonsView extends View
    {
        public static const ON_BUTTON_RESTART_TRIGGERED:String = "on_button_restart_triggered";

        [Inject]
        public var assetUtil:IAssetUtil;

        private var _button:Button;

        [PostConstruct]
        override public function initialize():void
        {
            var textFormat:TextFormat = new TextFormat(FontName.MAIN_FONT, 20, Color.WHITE);
            _button = new Button(assetUtil.getTexture(TextureName.BUTTON_RESTART), "МЕНЮ");
            _button.addEventListener(Event.TRIGGERED, onButtonRestartTriggered);
            _button.textFormat = textFormat;
            _button.alignPivot();
            _button.x = 100;
            _button.y = 55;
            addChild(_button);
        }

        private function onButtonRestartTriggered(e:Event):void
        {
            dispatchEventWith(ON_BUTTON_RESTART_TRIGGERED);
        }
    }
}
