package view.controls
{
    import starling.display.Image;
    import starling.display.Quad;
    import starling.utils.Color;

    import util.IAssetUtil;

    import view.View;
    import view.controls.buttons.ButtonsView;
    import view.controls.score.ScoreView;
    import view.controls.turn.TurnView;
    import view.root.RootView;

    public class ControlsView extends View
    {
        [Inject]
        public var assetUtil:IAssetUtil;

        private var _lineThickness:Number;
        private var _backGround:Quad;
        private var _turnView:TurnView;
        private var _scoreView:ScoreView;
        private var _buttonsView:ButtonsView;

        public function init(height:Number):void
        {
            _backGround = new Quad(RootView.stageWidth, height, 0x494949);
            addChild(_backGround);

            _lineThickness = 10;

            var topLine:Image = assetUtil.getScale9Image("gray_line");
            topLine.width = _backGround.width;
            topLine.height = _lineThickness + 1;
            topLine.y = -1;
            addChild(topLine);

            addVerticalLine(_backGround.width / 3);
            addVerticalLine(_backGround.width * 2 / 3);

            _turnView = injector.getOrCreateNewInstance(TurnView);
            addChild(_turnView);

            _scoreView = injector.getOrCreateNewInstance(ScoreView);
            _scoreView.x = RootView.stageWidth / 3;
            addChild(_scoreView);

            _buttonsView = injector.getOrCreateNewInstance(ButtonsView);
            _buttonsView.x = RootView.stageWidth * 2 / 3;
            addChild(_buttonsView);
    }

        private function addVerticalLine(x:Number):void
        {
            var verticalLine:Quad = new Quad(_lineThickness, _backGround.height - _lineThickness, 0xCACACA);
            verticalLine.y = _lineThickness;
            verticalLine.x = x - verticalLine.width * .5;
            addChild(verticalLine);
        }

        public function setTurnWhite():void
        {
            _turnView.setText("ХОД БЕЛЫХ");
            _turnView.setColor(Color.WHITE);
        }

        public function setTurnBlack():void
        {
            _turnView.setText("ХОД ЧЕРНЫХ");
            _turnView.setColor(Color.BLACK);
        }

        public function setScore(scoreBlack:int, scoreWhite:int):void
        {
            _scoreView.setScore(scoreBlack, scoreWhite);
        }

        public function get buttonsView():ButtonsView
        {
            return _buttonsView;
        }
    }
}
