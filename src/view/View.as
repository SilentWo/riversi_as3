package view
{
    import robotlegs.bender.framework.api.IInjector;

    import starling.display.Sprite;

    public class View extends Sprite
    {
        [Inject]
        public var injector:IInjector;

        [PostConstruct]
        public function initialize():void
        {
            // override this method with [PostConstruct]
        }
    }
}
