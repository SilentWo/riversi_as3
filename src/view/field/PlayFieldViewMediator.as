package view.field
{
    import controller.events.GameEvent;
    import controller.events.PlayFieldEvent;
    import controller.playfield.IPlayFieldObjectManager;

    import model.game.cell.CellModel;

    import model.game.cell.CellModel;
    import model.consts.CellState;

    import robotlegs.starling.bundles.mvcs.Mediator;

    import starling.events.Event;

    import view.field.cells.CellView;

    public class PlayFieldViewMediator extends Mediator
    {
        [Inject]
        public var view:PlayFieldView;
        [Inject]
        public var playFieldObjectManager:IPlayFieldObjectManager;

        override public function initialize():void
        {
            trace("PlayField view created");

            playFieldObjectManager.init(view.cellsContainer, view.cellSize);

            addViewListener(CellView.LIGHT_TOUCHED, onLightTouched)
        }

        private function onLightTouched(e:Event):void
        {
            var cell:CellModel = CellModel(e.data);
            dispatch(new GameEvent(GameEvent.MAKE_STEP, cell));
        }
    }
}
