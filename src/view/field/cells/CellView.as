package view.field.cells
{
    import model.game.cell.CellModel;
    import model.consts.CellState;

    import starling.display.Image;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.textures.Texture;

    import util.IAssetUtil;

    public class CellView extends Image
    {
        public static const LIGHT_TOUCHED:String = "LIGHT_TOUCHED";

        [Inject]
        public var assetUtil:IAssetUtil;

        private var _blackTexture:Texture;
        private var _whiteTexture:Texture;
        private var _allowTexture:Texture;
        private var _emptyTexture:Texture;
        private var _model:CellModel;

        public function CellView(blackTexture:Texture, whiteTexture:Texture, allowTexture:Texture):void
        {
            _blackTexture = blackTexture;
            _whiteTexture = whiteTexture;
            _allowTexture = allowTexture;
            super(_blackTexture);

            addEventListener(TouchEvent.TOUCH, onTouchHandler);
        }

        private function onTouchHandler(e:TouchEvent):void
        {
            if (_model == null || _model.state != CellState.ALLOW) return;

            var touch:Touch = e.getTouch(this, TouchPhase.BEGAN);
            if (touch != null)
            {
                dispatchEventWith(LIGHT_TOUCHED, true, _model);
            }
        }

        public function setModel(value:CellModel):void
        {
            _model = value;

            if (_model.state == CellState.BLACK)
            {
                texture = _blackTexture;
            } else if (_model.state == CellState.WHITE)
            {
                texture = _whiteTexture;
            } else if (_model.state == CellState.ALLOW)
            {
                texture = _allowTexture;
            } else if (_model.state == CellState.EMPTY)
            {
                texture = _emptyTexture;
            }
        }

        public function clearModel():void
        {
            _model = null;
        }

        public function get model():CellModel
        {
            return _model;
        }
    }
}
