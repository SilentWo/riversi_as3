package view.field.cells
{
    public interface ICellsFactory
    {
        function setCellSize(size:Number):void;

        function getCellView():CellView;

        function releaseCellView(cellView:CellView):void;
    }
}
