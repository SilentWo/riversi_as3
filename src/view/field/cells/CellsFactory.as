package view.field.cells
{
    import robotlegs.bender.framework.api.IInjector;

    import starling.textures.Texture;

    import util.IAssetUtil;
    import util.TextureName;

    public class CellsFactory implements ICellsFactory
    {
        [Inject]
        public var injector:IInjector;
        [Inject]
        public var assetUtil:IAssetUtil;

        private var _cellSize:Number;
        private var _pool:Vector.<CellView> = new <CellView>[];

        public function CellsFactory()
        {

        }

        public function getCellView():CellView
        {
            if (_pool.length > 0)
            {
                return _pool.pop();
            } else
            {
                var blackTexture:Texture = assetUtil.getTexture(TextureName.CHIP_BLACK);
                var whiteTexture:Texture = assetUtil.getTexture(TextureName.CHIP_WHITE);
                var allowTexture:Texture = assetUtil.getTexture(TextureName.CHIP_ALLOW);
                var cellView:CellView = new CellView(blackTexture, whiteTexture, allowTexture);
                cellView.alignPivot();
                return cellView;
            }
        }

        public function releaseCellView(cellView:CellView):void
        {
            cellView.removeFromParent();
            cellView.x = cellView.y = 0;
            cellView.alpha = 1;
            cellView.clearModel();

            if (_pool.indexOf(cellView) == -1)
                _pool[_pool.length] = cellView;
        }

        public function setCellSize(size:Number):void
        {
            _cellSize = size;
        }
    }
}
