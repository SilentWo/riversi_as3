package view.field
{
    import starling.display.Image;
    import starling.display.Quad;
    import starling.display.Sprite;
    import starling.text.TextField;
    import starling.text.TextFieldAutoSize;
    import starling.text.TextFormat;
    import starling.utils.Color;

    import util.FontName;
    import util.IAssetUtil;
    import util.TextureName;

    import view.View;
    import view.root.RootView;

    public class PlayFieldView extends View
    {
        public static const SIZE_IN_CELLS:int = 8;

        [Inject]
        public var assetUtil:IAssetUtil;

        private var _cellSize:Number;
        private var _halfCellSize:Number;
        private var _frameThickness:Number;
        private var _field:Sprite;
        private var _cellsContainer:Sprite;

        [PostConstruct]
        override public function initialize():void
        {
            _frameThickness = 20;

            _field = new Sprite();
            addChild(_field);

            var fieldSize:Number = RootView.stageWidth - 2 * _frameThickness;
            var fieldBackGround:Quad = new Quad(fieldSize, fieldSize, 0xABCF96);
            _field.addChild(fieldBackGround);

            _cellSize = fieldSize / SIZE_IN_CELLS;
            _halfCellSize = _cellSize * .5;

            const LINE_WIDTH:Number = 3;
            const LINE_COLOR:uint = 0x7BB700;

            for (var i:int = 1; i < SIZE_IN_CELLS; i++)
            {
                var verticalLine:Quad = new Quad(LINE_WIDTH, fieldSize, LINE_COLOR);
                verticalLine.y = 0;
                verticalLine.x = i * _cellSize;
                _field.addChild(verticalLine);

                var horizontalLine:Quad = new Quad(fieldSize, LINE_WIDTH, LINE_COLOR);
                horizontalLine.y = i * _cellSize;
                horizontalLine.x = 0;
                _field.addChild(horizontalLine);
            }

            var frame:Image = assetUtil.getScale9Image(TextureName.PLAYFIELD_FRAME);
            frame.width = frame.height = RootView.stageWidth;
            frame.alignPivot();
            frame.x = frame.y = frame.width * .5;
            addChild(frame);

            _field.alignPivot();
            _field.x = _field.y = frame.y;

            // scales
            var topScale:Sprite = getVerticalScale();
            topScale.x = _frameThickness;
            addChild(topScale);

            var bottomScale:Sprite = getVerticalScale();
            bottomScale.x = topScale.x;
            bottomScale.y = frame.height - _frameThickness;
            addChild(bottomScale);

            var leftScale:Sprite = getHorizontalScale();
            leftScale.y = _frameThickness - _cellSize * .3;
            addChild(leftScale);

            var rightScale:Sprite = getHorizontalScale();
            rightScale.x = frame.width - _frameThickness;
            rightScale.y = leftScale.y;
            addChild(rightScale);

            _cellsContainer = new Sprite();
            _cellsContainer.x = _field.x - _field.width * .5;
            _cellsContainer.y = _field.y - _field.height * .5;
            addChild(_cellsContainer);
        }

        private function getVerticalScale():Sprite
        {
            const SCALE_NUMBER:String = "12345678";

            var sprite:Sprite = new Sprite();
            var labelHeight:Number = 70;
            var textFormat:TextFormat = new TextFormat(FontName.GREEN_SCALE, labelHeight, Color.WHITE);

            for (var i:int = 0; i < 8; i++)
            {
                var textField:TextField = new TextField(_cellSize, 1, SCALE_NUMBER.charAt(i), textFormat);
                textField.x = i * _cellSize;
                textField.autoSize = TextFieldAutoSize.VERTICAL;
                textField.autoScale = true;
                sprite.addChild(textField);
            }
            return sprite;
        }

        private function getHorizontalScale():Sprite
        {
            const SCALE_LETTERS:String = "ABCDEFGH";

            var sprite:Sprite = new Sprite();
            var labelHeight:Number = 90;
            var textFormat:TextFormat = new TextFormat(FontName.GREEN_SCALE, labelHeight, Color.WHITE);

            for (var i:int = 0; i < 8; i++)
            {
                var textField:TextField = new TextField(1, _cellSize, SCALE_LETTERS.charAt(i), textFormat);
                textField.y = i * _cellSize;
                textField.autoSize = TextFieldAutoSize.HORIZONTAL;
                textField.autoScale = true;
                sprite.addChild(textField);
            }
            return sprite;
        }

        public function get cellsContainer():Sprite
        {
            return _cellsContainer;
        }

        public function get cellSize():Number
        {
            return _cellSize
        }
    }
}
