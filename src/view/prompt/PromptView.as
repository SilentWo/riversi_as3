package view.prompt
{

    import com.greensock.TweenLite;

    import starling.text.TextField;
    import starling.text.TextFieldAutoSize;
    import starling.text.TextFormat;
    import starling.utils.Color;

    import util.FontName;

    import util.IAssetUtil;

    import view.View;
    import view.root.RootView;

    public class PromptView extends View
    {
        [Inject]
        public var assetUtil:IAssetUtil;

        private var _textField:TextField;

        [PostConstruct]
        override public function initialize():void
        {
            this.touchable = false;

            var labelHeight:Number = 40;
            var textFormat:TextFormat = new TextFormat(FontName.MAIN_FONT, labelHeight, Color.WHITE);

            _textField = new TextField(RootView.stageWidth, labelHeight, "", textFormat);
            _textField.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
            addChild(_textField);
        }

        public function setPrompt(text:String):void
        {
            _textField.text = text;
            _textField.alignPivot();
            _textField.x = RootView.stageWidth * .5;
            _textField.y = 25;
            _textField.format.color = Color.YELLOW;

            TweenLite.killDelayedCallsTo(clearPrompt);
            TweenLite.delayedCall(3, clearPrompt);
        }

        private function clearPrompt():void
        {
            _textField.text = ""
        }
    }
}
