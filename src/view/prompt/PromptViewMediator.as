package view.prompt
{
    import controller.events.GameEvent;

    import robotlegs.starling.bundles.mvcs.Mediator;

    public class PromptViewMediator extends Mediator
    {
        [Inject]
        public var view:PromptView;

        override public function initialize():void
        {
            addContextListener(GameEvent.SHOW_PROMPT, onShowPromptHandler, GameEvent);
        }

        private function onShowPromptHandler(e:GameEvent)
        {
            if (e.data == null || e.data.text == null) return;


            view.setPrompt(e.data.text);
        }
    }
}
