package view.root
{
    import controller.events.GameEvent;

    import robotlegs.bender.framework.api.IInjector;
    import robotlegs.starling.bundles.mvcs.Mediator;

    import starling.events.Event;

    public class RootViewMediator extends Mediator
    {
        [Inject]
        public var view:RootView;
        [Inject]
        public var injector:IInjector;

        override public function initialize():void
        {
            injector.injectInto(view);

            trace("Root view created");

            addContextListener(GameEvent.CREATE_GAME_VIEWS, onCreateGameViews);
            addContextListener(GameEvent.SHOW_START_MENU, onShowStartMenu);
            addContextListener(GameEvent.HIDE_START_MENU, onHideStartMenu);

            dispatch(new GameEvent(GameEvent.INIT_APPLICATION));
        }

        private function onCreateGameViews(e:GameEvent):void
        {
            view.init();
        }

        private function onShowStartMenu(e:GameEvent):void
        {
            view.showStartMenu();
        }

        private function onHideStartMenu(e:GameEvent):void
        {
            view.hideStartMenu();
        }
    }
}
