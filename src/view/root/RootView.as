package view.root
{
    import starling.display.Quad;
    import starling.utils.Color;

    import view.View;
    import view.controls.ControlsView;
    import view.field.PlayFieldView;
    import view.menu.StartMenuView;
    import view.prompt.PromptView;

    public class RootView extends View
    {
        private static var _stageWidth:Number;
        private static var _stageHeight:Number;

        private var _playFieldView:PlayFieldView;
        private var _controlsView:ControlsView;
        private var _startMenuView:StartMenuView;
        private var _startMenuViewOverlay:Quad;
        private var _promptView:PromptView;

        public function init():void
        {
            _playFieldView = injector.getOrCreateNewInstance(PlayFieldView);
            addChild(_playFieldView);

            _controlsView = injector.getOrCreateNewInstance(ControlsView);
            _controlsView.y = _playFieldView.height;
            _controlsView.init(stageHeight - _playFieldView.height);
            addChild(_controlsView);

            _promptView = injector.getOrCreateNewInstance(PromptView);
            addChild(_promptView);
        }

        public static function get stageWidth():Number
        {
            return _stageWidth;
        }

        public static function set stageWidth(value:Number):void
        {
            if (!isNaN(_stageWidth))
                throw new ArgumentError("Stage size already initiated!");
            _stageWidth = value;
        }

        public static function get stageHeight():Number
        {
            return _stageHeight;
        }

        public static function set stageHeight(value:Number):void
        {
            if (!isNaN(_stageHeight))
                throw new ArgumentError("Stage size already initiated!");
            _stageHeight = value;
        }

        public function showStartMenu():void
        {
            if (_startMenuView == null)
            {
                _startMenuView = injector.getOrCreateNewInstance(StartMenuView);
                _startMenuView.alignPivot();
                _startMenuView.x = stageWidth * .5;
                _startMenuView.y = stageHeight * .42;

                _startMenuViewOverlay = new Quad(stageWidth, stageHeight, Color.BLACK);
                _startMenuViewOverlay.alpha = .5;
            }
            addChild(_startMenuViewOverlay);
            addChild(_startMenuView);
        }

        public function hideStartMenu():void
        {
            if (_startMenuView != null)
                removeChild(_startMenuView);
            if (_startMenuViewOverlay != null)
                removeChild(_startMenuViewOverlay);
        }
    }
}
