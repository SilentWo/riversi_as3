package view.menu
{
    import starling.display.Button;
    import starling.display.Image;
    import starling.events.Event;
    import starling.text.TextField;
    import starling.text.TextFormat;
    import starling.utils.Color;

    import util.FontName;

    import util.IAssetUtil;
    import util.TextureName;

    import view.View;

    public class StartMenuView extends View
    {
        public static const MODE_PLAYERS:String = "modePlayers";
        public static const MODE_SIDE:String = "modeSide";
        public static const ON_LEFT_BUTTON_TRIGGERED:String = "on_left_button_triggered";
        public static const ON_RIGHT_BUTTON_TRIGGERED:String = "on_right_button_triggered";

        [Inject]
        public var assetUtil:IAssetUtil;

        private var _caption:TextField;
        private var _leftButton:Button;
        private var _rightButton:Button;
        private var _mode:String;

        [PostConstruct]
        override public function initialize():void
        {
            var backGround:Image = assetUtil.getScale9Image(TextureName.WINDOW_BG);
            backGround.width = 300;
            backGround.height = 150;
            addChild(backGround);

            var labelHeight:Number = 23;
            var textFormat:TextFormat = new TextFormat(FontName.MAIN_FONT, labelHeight, Color.WHITE);

            _caption = new TextField(300, 50);
            _caption.format = textFormat;
            addChild(_caption);

            _leftButton = new Button(assetUtil.getTexture(TextureName.BUTTON_CHOISE));
            _leftButton.addEventListener(Event.TRIGGERED, onLeftButtonTriggered);
            _leftButton.textFormat = textFormat;
            _leftButton.textFormat.size = 18;
            _leftButton.alignPivot();
            _leftButton.x = 80;
            _leftButton.y = 105;
            addChild(_leftButton);

            _rightButton = new Button(assetUtil.getTexture(TextureName.BUTTON_CHOISE));
            _rightButton.addEventListener(Event.TRIGGERED, onRightButtonTriggered);
            _rightButton.textFormat = textFormat;
            _rightButton.textFormat.size = _leftButton.textFormat.size;
            _rightButton.alignPivot();
            _rightButton.x = backGround.width - _leftButton.x;
            _rightButton.y = _leftButton.y;
            addChild(_rightButton);
        }

        public function setMode(mode:String):void
        {
            _mode = mode;
            if (mode == StartMenuView.MODE_PLAYERS)
            {
                _caption.text = "КОЛИЧЕСТВО ИГРОКОВ";
                _leftButton.text = "1";
                _leftButton.textFormat.color = Color.WHITE;
                _rightButton.text = "2";
            } else
            {
                _caption.text = "ВЫБЕРИТЕ СТОРОНУ";
                _leftButton.text = "Ч";
                _leftButton.textFormat.color = Color.GRAY;
                _rightButton.text = "Б";
            }

            _caption.alignPivot();
            _caption.x = 150;
            _caption.y = 38;
        }

        private function onLeftButtonTriggered(e:Event):void
        {
            dispatchEventWith(ON_LEFT_BUTTON_TRIGGERED);
        }

        private function onRightButtonTriggered(e:Event):void
        {
            dispatchEventWith(ON_RIGHT_BUTTON_TRIGGERED);
        }

        public function isPlayersMode():Boolean
        {
            return _mode == MODE_PLAYERS;
        }
    }
}
