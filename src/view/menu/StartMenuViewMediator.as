package view.menu
{
    import controller.events.GameEvent;
    import controller.events.GameEvent;

    import model.consts.CellState;

    import robotlegs.starling.bundles.mvcs.Mediator;

    import starling.events.Event;

    public class StartMenuViewMediator extends Mediator
    {
        [Inject]
        public var view:StartMenuView;

        override public function initialize():void
        {
            view.setMode(StartMenuView.MODE_PLAYERS);
            addViewListener(StartMenuView.ON_LEFT_BUTTON_TRIGGERED, onButtonTriggered);
            addViewListener(StartMenuView.ON_RIGHT_BUTTON_TRIGGERED, onButtonTriggered);
        }

        private function onButtonTriggered(e:Event):void
        {
            if (view.isPlayersMode())
            {
                if (e.type == StartMenuView.ON_LEFT_BUTTON_TRIGGERED)
                {
                    view.setMode(StartMenuView.MODE_SIDE);
                } else
                {
                    dispatch(new GameEvent(GameEvent.START_GAME, {players: 2}));
                    hideView();
                }
            } else // select side mode
            {
                var side:int;
                if (e.type == StartMenuView.ON_LEFT_BUTTON_TRIGGERED)
                {
                    side = CellState.BLACK;
                } else
                {
                    side = CellState.WHITE;
                }
                dispatch(new GameEvent(GameEvent.START_GAME, {players: 1, side: side}));
                hideView();
            }
        }

        private function hideView():void
        {
            dispatch(new GameEvent(GameEvent.HIDE_START_MENU));
        }
    }
}
