package
{
    import core.ApplicationContext;

    import flash.display.Sprite;

    [SWF(width="600", height="700", frameRate="60", backgroundColor="#ffffff")]
    public class Main extends Sprite
    {
        private var _context:ApplicationContext;

        public function Main()
        {
            _context = new ApplicationContext(stage);
        }
    }
}
